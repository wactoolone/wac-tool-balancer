package com.wearechurch.tool.balancer.controller;

import org.springframework.web.bind.annotation.PostMapping;

import com.wearechurch.tool.balancer.dto.Version;
import com.wearechurch.tool.balancer.tool.BalancerConstant;
import com.wearechurch.tool.dto.Response;

public interface IVersionController {

	@PostMapping(BalancerConstant.VERSION_VALIDATE)
	Response<Void> validateVersion(final Version version);
}
