package com.wearechurch.tool.balancer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodyUriSpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;

import com.wearechurch.tool.balancer.configuration.BalancerProperty;
import com.wearechurch.tool.balancer.enumerator.Server;
import com.wearechurch.tool.dto.Response;

@Component
public class BalancerService {

	@Autowired
	private BalancerProperty property;

	@Autowired
	private WebClient.Builder builder;

	private <R> Response<R> body(final Object request, final Server server, final String uri, final Class<R> response,
			final RequestBodyUriSpec spec) {
		return spec.uri(uri(server, uri)).bodyValue(request).retrieve().bodyToMono(reference(response)).block();
	}

	private WebClient build(final Server server) {
		String username;
		String password;
		if (server == Server.ADMINISTRATION) {
			username = property.getServiceNameAdministration();
			password = property.getServicePasswordAdministration();
		} else {
			username = property.getServiceNameData();
			password = property.getServicePasswordData();
		}
		return builder.filter(ExchangeFilterFunctions.basicAuthentication(username, password)).build();
	}

	public <R> Response<R> delete(final Server server, final String uri, final Class<R> response) {
		build(server).delete();
		return header(server, uri, response, build(server).delete());
	}

	public <R> Response<R> get(final Server server, final String uri, final Class<R> response) {
		return header(server, uri, response, build(server).get());
	}

	private <R> Response<R> header(final Server server, final String uri, final Class<R> response,
			final RequestHeadersUriSpec<?> spec) {
		return spec.uri(uri(server, uri)).retrieve().bodyToMono(reference(response)).block();
	}

	public <R> Response<R> post(final Object request, final Server server, final String uri, final Class<R> response) {
		return body(request, server, uri, response, build(server).post());
	}

	public <R> Response<R> put(final Object request, final Server server, final String uri, final Class<R> response) {
		return body(request, server, uri, response, build(server).put());
	}

	private <R> ParameterizedTypeReference<Response<R>> reference(final Class<R> response) {
		return ParameterizedTypeReference
				.forType(ResolvableType.forClassWithGenerics(Response.class, response).getType());
	}

	private String uri(final Server server, final String uri) {
		return new StringBuilder().append("http://WAC-SERVER-").append(server.name()).append("/").append(uri)
				.toString();
	}
}
