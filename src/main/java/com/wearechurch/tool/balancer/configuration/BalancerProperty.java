package com.wearechurch.tool.balancer.configuration;

import org.springframework.beans.factory.annotation.Value;

import com.wearechurch.tool.annotation.ComponentConfiguration;

@ComponentConfiguration
public class BalancerProperty {

	@Value("${service.name.administration}")
	private String serviceNameAdministration;

	@Value("${service.name.data}")
	private String serviceNameData;

	@Value("${service.password.administration}")
	private String servicePasswordAdministration;

	@Value("${service.password.data}")
	private String servicePasswordData;

	public String getServiceNameAdministration() {
		return serviceNameAdministration;
	}

	public String getServiceNameData() {
		return serviceNameData;
	}

	public String getServicePasswordAdministration() {
		return servicePasswordAdministration;
	}

	public String getServicePasswordData() {
		return servicePasswordData;
	}

}
